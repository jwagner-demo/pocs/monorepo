# monorepo

This project is a proof-of-concept for a monorepo application with 3 separate project folders that would like to run pipelines based on which project changes.

## How It Works

The `.gitlab-ci.yml` file in the project root defines the stages for the pipeline and then includes the `.gitlab-ci.yml` file for each Project A, Project B, and Project C.

Inside each of the project specific `.gitlab-ci.yml` file, we utilize the `rules:changes` keyword to ensure jobs only run when changes have been made inside of their respective projects. 

**Documentation Links**
- [Includes Documentation](https://docs.gitlab.com/ee/ci/yaml/index.html#include)
- [rules:changes Documentation](https://docs.gitlab.com/ee/ci/yaml/index.html#ruleschanges)

## Prerequisites

When utilizing this flow, it's important to have the correct permissions set up.

It's recommended to [require everyone to submit a merge request for the main branch](https://docs.gitlab.com/ee/user/project/protected_branches.html#require-everyone-to-submit-merge-requests-for-a-protected-branch) so that you can set up specific [merge Request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/) to approve the merge into `main` and deployment to production.

It's also recommended to [require a successful pipeline to approve a merge request](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html#require-a-successful-pipeline-for-merge).

## Recommended Workflow

<div class="center">

```mermaid
gitGraph
       commit
       branch feature-a
       checkout feature-a
       commit
       commit
       commit
       checkout main
       merge feature-a
       commit
       branch feature-b
       checkout feature-b
       commit
       commit
       commit
       checkout main
       merge feature-b
```
</div>

1. Create an Issue
2. Create a Merge Request from the Issue. This will create a Merge Request and a feature branch for the issue.
3. Make & commit changes on the feature branch. This will run the pipeline and deploy to staging, but stop before deploying to production.
4. Approve merge request for the feature branch into main to deploy to production.

## Pipeline Stages
1. Build - This stage is responsible for building the project(s) that have changed. It runs on all changes.
2. Test - This stage is responsible for testing the project(s) that have changed. It should be used to run tests that are not dependent on an environment (e.g. unit tests). It runs on all changes.
3. Staging - This stage is responsible for deploying the project(s) that have changed to the staging environment. It runs on all changes except the `main` branch.
4. System Test - This stage is responsible for testing the project(s) that have changed inside of the staging environment. It should be used to run all tests that are dependent on an environment (e.g. API Tests, Regression Tests, etc.) It runs on all changes except the `main` branch.
4. Production - This stage is responsible for deploying the project(s) that have changed to the production environment. It only runs on the `main` branch.

## Example Pipelines

**Single Project Update**
- [Staging Pipeline](https://gitlab.com/jwagner-demo/pocs/monorepo/-/pipelines/670260189)
- [Production Pipeline](https://gitlab.com/jwagner-demo/pocs/monorepo/-/pipelines/670260793)

**Multiple Project Update**
- [Staging Pipeline](https://gitlab.com/jwagner-demo/pocs/monorepo/-/pipelines/670261484)
- [Production Pipeline](https://gitlab.com/jwagner-demo/pocs/monorepo/-/pipelines/670263663)



